#!/usr/bin/env bash

inp=$1
echo "--------set ubuntu ganglia worker MUST run as sudo, apt-get ON=$inp"
if [ x$inp != x ]; then
    apt-get install ganglia-monitor
fi
FILE=/etc/ganglia/gmond.conf
if [ -f $FILE ];
then
   echo "File $FILE exists."
else
   echo "File $FILE does not exist."
   echo run script again with installation of ganglia ON
   exit
fi

service ganglia-monitor stop
cp gmond.conf.blades /etc/ganglia/gmond.conf 
#cp gmond.conf.vm-stackE /etc/ganglia/gmond.conf 
service ganglia-monitor start